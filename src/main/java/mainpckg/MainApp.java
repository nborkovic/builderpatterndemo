package mainpckg;

public class MainApp {

    public static void main(String[] args) {

        Builder builder = new UserBuilder();
        Director director = new Director(builder);
        User user1 = director.construct().build();
        System.err.println(user1);
    }
}
