package mainpckg;

public class Director {

    Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public UserBuilder construct(){
        return this.builder.buildName("Jure")
                      .buildAddress("Kutina")
                      .buildPhone("040545453");
    }
}
