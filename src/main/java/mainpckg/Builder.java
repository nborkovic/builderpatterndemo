package mainpckg;

public interface Builder {

    public UserBuilder buildName(String name);

    public UserBuilder buildAddress(String address);

    public UserBuilder buildPhone(String phone);

    public User build();
}
