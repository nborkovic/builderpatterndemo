package mainpckg;

public class UserBuilder implements Builder {

    private User user;

    public UserBuilder() {
        setUser(new User());
    }

    private User getUser() {
        return user;
    }

    private void setUser(User user) {
        this.user = user;
    }

    public UserBuilder buildName(String name){
        this.user.setName(name);
        return this;
    }

    public UserBuilder buildAddress(String address){
        this.user.setAddress(address);
        return this;
    }

    public UserBuilder buildPhone(String phone){
        this.user.setPhone(phone);
        return this;
    }

    public User build(){
        return getUser();
    }
}
